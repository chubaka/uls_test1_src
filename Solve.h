#ifndef _SOLVE_H
#define _SOLVE_H

#include <algorithm>
#include <array>

#include "Point.h"
typedef std::array<std::size_t, 3> 	out_value;
typedef std::vector<Point2> 		in_value;

class Solve {
public:
	Solve();
	~Solve();

public:
	out_value solve ( const in_value & points);
};







#endif

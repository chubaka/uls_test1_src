#include <iostream>

#include "Solve.h"


Solve::Solve(){ }

Solve::~Solve() {

}
//----------------------------------------------------------------------------------------------------------------------

long calc_square(const Point2 & a, const Point2 & b, const Point2 & c)
{
   return abs((a.x - c.x)*(b.y - c.y) + (b.x-c.x)*(c.y-a.y));
}

out_value Solve::solve ( const in_value & points) {
	out_value ret = {0,0,0};

	//maximize performance, because 1e5 points is a huge amount
	//access to the [] element is faster than the iterator
	//and const is much faster
	const auto endk = &points[points.size() - 1];
	const auto endj = &points[points.size() - 2];
	const auto endi = &points[points.size() - 3];

	for (const in_value::value_type * i = &points[0]; i <= endi; ++i) {
		for (const in_value::value_type * j = i + 1; j <= endj; ++j) {
			for (const in_value::value_type * k = j + 1; k <= endk; ++k) {
				//check if 3 points is on the one line
				//and additional if square > 0
				long q = 0;
				if ( ( (k->x - i->x) / (j->x - i->x) !=
						(k->y - i->y) / (j->y - i->y) ) &&
						( q = calc_square(*i, *j, *k) ) > 0) {
					//we have found the wanted triangle
					//but we need to check all point are not in it
					bool found = true;
					for (const in_value::value_type * t = &points[0]; t <= endk; ++t) {
						if (t == i || t == j || t == k)
							continue;

						if (q == calc_square(*i, *j, *t)
								+ calc_square(*i, *t, *k)
								+ calc_square(*j,*t,*k))
						{
							//abort :( there is a point in the triangle
							found = false;
							break;
						}
					}
					if (found) {
						ret[0] = i - &points[0];
						ret[1] = j - &points[0];
						ret[2] = k - &points[0];
						return ret;
					}
				}
			}
		}
	}
	return ret;
}

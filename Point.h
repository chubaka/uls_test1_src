#ifndef _POINT_H
#define _POINT_H


struct Point2 {
	int x;
	int y;

	Point2()
	: x(0),
	  y(0) {}

	int & operator[] (int i)
	{
		return i == 0 ? x : y;
	}
};



#endif

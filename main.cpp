//include
#include <iostream>

//local include
#include "Run.h"

//defines
#define RC_OK(x) x == 0 ? 1 : 0

using namespace std;

source_type parse(int argc, const char ** argv, /*out*/std::string & s) {
	source_type t = source_type::File;
	if (argc == 2) { //assume that source is file
		s.assign(argv[1]);
	} else if (argc > 2) { //assume that source is number of points
		//like 1 -2 45 12 -85 6 15 22 (pair x, y)
		for (int i = 1; i < argc; ++i) {
			s+= ' ';
			s.append(argv[i]);
		}
		t = source_type::String;
	}
	return t;
}

int main (int argc, const char ** argv) {
	try {
		//Incoming data parser
		std::string s;
		source_type t = parse(argc, argv, s);

		Run run(t,s);
		while (RC_OK(run.cycle())) {
			cout << "Program starts again." << endl;
		}
	} catch (...) {
		return -1;
	}
	return 0;
}

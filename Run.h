#ifndef _RUN_H
#define _RUN_H

#include "Solve.h"

enum source_type {
	File,
	String
};

class Run {
public:
	Run(source_type _t, std::string & source);
	~Run();

	int cycle();
	bool parse();

private:
	Solve m_solve;
	std::vector<Point2> m_vector;

	source_type m_type;
	std::string m_source;
};



#endif

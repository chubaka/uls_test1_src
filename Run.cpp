#include "Run.h"

#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <iomanip> // setw

Run::Run(source_type _t, std::string & _source)
	: m_type(_t),
	  m_source(_source)
{ }

Run::~Run() { }

std::string::iterator & get_point(
		/*in*/ std::string::iterator & str,
		/*out*/ Point2 & p)
{
	for (int i = 0; i < 2; ++i) { //point coord
		char * beg = nullptr;
		char * end = nullptr;

		while ( *str) {
			if ( *str == ' ') {
				++str;
				continue;
			} else {
				if ( !beg) {
					beg = &*str;
					while (*str != ' ' && *str)
						++str;
					end = &*str;
					char buf[10] = {0};
					std::memcpy(buf, beg, end - beg);
					p[i] = std::stoi(buf);
					break;
				}
			}
		}
	}
	return str;
}

void draw_point(const Point2 & p) {
	std::cout << std::setw(11) << p.x << " , " << p.y << std::endl;
}

//1 1 2 -3 44 89 55 12
bool Run::parse()
{
	m_vector.clear();
	std::cout << "parsing\n";
	if (m_type == source_type::File) { // file
		std::ifstream s;
		s.open(m_source.c_str());
		if (s.is_open()) {
			getline(s, m_source);
			s.close();
		}
	}

	std::string::iterator head = m_source.begin();
	std::string::iterator tail = m_source.end();
	while ( *head) {
		Point2 p;
		head = get_point( head, p);
		m_vector.push_back(p);
	}
	return m_vector.size() ? true : false;
}
//----------------------------------------------------------------------------------------------------------------------

int Run::cycle() {
	std::cout << "New iteration. Continue? [Y/N]";
	char c;
	std::cin >> c;

	if (c == 'N' || c == 'n')
		return 1;

	std::cout << "\nParsed:" << parse() << "; vector:\n";
	std::for_each (m_vector.begin(), m_vector.end(), draw_point);

	out_value ret = m_solve.solve(m_vector);

	if (!ret[0] && !ret[1] && !ret[2]) {
		std::cout << "\nSolution not found\n";
	} else {
		std::cout << "\nSolution points:\n";
		std::for_each(ret.begin(), ret.end(),
				[&](std::size_t & i) { std::cout << "Point #" << i << ": "; draw_point(m_vector[i]);}
		);
	}
	return 0;
}
